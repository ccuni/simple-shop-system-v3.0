package com.star.ms.common.service;


import com.star.ms.common.pojo.api.OssUploadResult;

import java.util.List;

public interface OssService {
    // 获取所有图片的路径
    public List<String[]> getDefaultHeadImgLink();
    // 上传文件
    public OssUploadResult upload(String userCode, String img64base);

    // 获取文件路径
    public String getUserImgPath(String userCode, String imgType);

    OssUploadResult uploadById(Long userId, String img64Base);
    public OssUploadResult uploadProduct(Long productId, String img64base);
    public String getProductImgPath(Long productId, String imgType);

}
