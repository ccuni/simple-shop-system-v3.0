//package com.star.ms.common.pojo.api;
//
////import com.alipay.api.domain.GoodsDetail;
////import com.alipay.api.domain.ReceiverAddressInfo;
//import lombok.Data;
//import lombok.Getter;
//import lombok.Setter;
//import org.springframework.format.annotation.DateTimeFormat;
//import org.springframework.format.annotation.NumberFormat;
//import java.util.Date;
//import java.util.List;
//
//@Getter
//@Setter
//public class AliPayOrder {
//    //////////////////////////////////////////////////////////////////// 订单信息
//    private String out_trade_no;  // 订单号
//    private String subject;     // 订单标题
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private Date time_expire;       // 过期时间
//    private String product_code = "FAST_INSTANT_TRADE_PAY";        //产品码。商家和支付宝签约的产品码。
//
////    private List<GoodsDetail[]> goods_detail;
////    private ReceiverAddressInfo receiver_address_info;                // 收货人基本信息
//    private Long buyer_id;       // 买家ID
//    private String seller_id;    // 卖家ID
//
//    @Override
//    public String toString() {
//        return "AliPayOrder{" +
//                "out_trade_no='" + out_trade_no + '\'' +
//                ", subject='" + subject + '\'' +
//                ", time_expire=" + time_expire +
//                ", product_code='" + product_code + '\'' +
//                ", buyer_id=" + buyer_id +
//                ", seller_id='" + seller_id + '\'' +
//                '}';
//    }
//}
