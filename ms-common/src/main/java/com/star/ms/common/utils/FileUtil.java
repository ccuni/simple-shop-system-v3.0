package com.star.ms.common.utils;

import com.alibaba.fastjson.JSON;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileUtil {
    public static  List<String> load(String resourcesFileName) throws IOException {
        ClassPathResource resource = new ClassPathResource(resourcesFileName);
        File file = resource.getFile();
        return FileUtils.readLines(file);
    }
}
