package com.star.ms.common.dao.mapper.provider;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.provider.Provider;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_provider】的数据库操作Mapper
* @createDate 2022-05-22 13:29:09
* @Entity com.star.ms.pojo.Provider
*/
@Repository
public interface ProviderMapper extends BaseMapper<Provider> {

}




