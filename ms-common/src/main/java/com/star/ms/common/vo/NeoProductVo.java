package com.star.ms.common.vo;

import com.star.ms.common.pojo.neo4j.NeoProduct;
import lombok.Data;

@Data
public class NeoProductVo {
    private NeoProduct neoProduct;
    private String img;
    private double price;
    private int count;
}
