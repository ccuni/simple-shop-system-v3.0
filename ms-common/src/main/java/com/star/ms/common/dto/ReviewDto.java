package com.star.ms.common.dto;

import lombok.Data;

/**
 * 用户发表评论时候前后端交互的实体类
 */

@Data
public class ReviewDto {
    private Long productId;         // 评论的商品ID
    private String reviewStar;      // 评论的星级
    private String reviewInfo;      // 评论的内容
}
