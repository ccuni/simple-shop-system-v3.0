package com.star.ms.common.enums;

import lombok.Getter;

/*
 * @Author: Uni
 * @Time: 2022/5/10
 * @TODO 关于商品状态的枚举
 */
@Getter
public enum ProductStatusEnum {
    MIN_PRICE("minPrice", "最小价格"),
    MAX_PRICE("maxPrice", "最高价格"),
    NAME("name", "商品名称"),
    PRODUCT_TYPE_ID("prodid", "商品类型的ID");
    private final String val;
    private final String detail;
    private ProductStatusEnum(String val, String detail){
        this.val = val;
        this.detail = detail;
    }
}
