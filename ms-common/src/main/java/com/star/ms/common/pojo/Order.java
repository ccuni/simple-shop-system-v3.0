package com.star.ms.common.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.provider.Provider;
import com.star.ms.common.pojo.user.User;
import lombok.Data;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
/**
 *
 * @TableName ms_order
 */
@TableName(value ="ms_order")
@Data
public class Order implements Serializable {
    /**
     * 订单ID
     */
    @TableId(value = "order_id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品的ID
     */
    @TableField(value = "order_product_id")
    private Long productId;

    /**
     * 买家的用户ID
     */
    @TableField(value = "order_buyer_id")
    private Long buyerId;

    /**
     * 供应商ID
     */
    @TableField(value = "order_provider_id")
    private Long providerId;

    /**
     * 成交价格
     */
    @TableField(value = "order_price")
    private Double price;

    /**
     * 交易数量
     */
    @TableField(value = "order_number")
    private Integer number;

    /**
     * 交易时间
     */
    @TableField(value = "order_create_time")
    private Date createTime;

    /**
     * 订单状态（0未支付，1已支付，2已完成）
     */
    @TableField(value = "order_status")
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    @TableField(exist = false)
    private User buyer;
    @TableField(exist = false)
    private Product product;
    @TableField(exist = false)
    private Provider provider;
}