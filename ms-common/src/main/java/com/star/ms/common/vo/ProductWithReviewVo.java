package com.star.ms.common.vo;


import com.star.ms.common.pojo.product.Product;
import lombok.Data;

//  查询不同商品种类的评论数
@Data
public class ProductWithReviewVo {
    private Long productId;
    private Product product;
    private Integer total;
}
