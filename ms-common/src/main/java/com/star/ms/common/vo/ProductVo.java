package com.star.ms.common.vo;

import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.provider.Provider;
import lombok.Data;

import java.util.List;

@Data
public class ProductVo {
    Product product;
    Provider provider;
    private List<Product> sames;
}
