package com.star.ms.common.dao.mapper.order;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.ms.common.pojo.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_order】的数据库操作Mapper
* @createDate 2022-06-07 07:33:29
* @Entity com.star.ms.common.pojo.Order
*/
@Repository
public interface OrderMapper extends BaseMapper<Order> {

    IPage<Order> selectWithBuyerAndProviderByPage(Page<Object> page, @Param("map") Map<String, Object> condition);

    Order selectWithBuyerAndProviderAndProduct(Long orderId);

    List<Order> selectOrderListByUserId(Long userId, Integer status);

    List<Order> selectCurrentOrders(Integer num);

    Double selectCountMoney();

    Float selectCountMoneyByCategoryId(Long categoryId);
}




