package com.star.ms.common.pojo.neo4j;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NeoReview {
    private Long id;
    private String reviewInfo;
    private Long userId;
    private Long productId;
}
