package com.star.ms.admin.dao.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.pojo.api.Captcha;
import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.user.User;
import com.star.ms.common.vo.UserProvinceCountVo;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;


public interface UserDAO {
    public User insertUser(User newUser) ;
    public User selectByCodeAndPassword(String code, String password);
    public boolean insertCaptcha(User user, Captcha captcha);
    public Captcha selectCaptcha(User user);

    public User selectByCode(String code);

    public boolean updateSelective(User user);

    int updateImgByCode(String code, String imgUrl);

    List<User> selectWithRoleByMap(Map<String, Object> condition);

    IPage<Product> selectListWithRoleBySome(Page<Object> objectPage, Map<String, Object> condition);

    List<User> selectWithRoleByIds(List<Long> ids);

    int updateImgById(Long userId, String imgUrl);

    int selectMaxUserId();

    public User selectById(Long userId);

    public List<UserProvinceCountVo> selectListWithProvinceCount();

    boolean updatePassword(User user);
}
