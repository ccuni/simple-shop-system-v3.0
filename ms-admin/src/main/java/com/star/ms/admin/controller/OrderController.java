package com.star.ms.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.star.ms.admin.service.order.OrderService;
import com.star.ms.common.pojo.Order;
import com.star.ms.common.pojo.RestResponse;
import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.provider.Provider;
import com.star.ms.common.pojo.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;


    /**
     * 查询商品信息
     * @param param
     * @return
     */
    @PostMapping("/query")
    public RestResponse<IPage<Order>> getOrder(@RequestBody(required = false) Map<String, Object> condition){
        int pageNum = 1;
        int pageSize = 5;
        // 处理空参
        if(condition != null){
            pageNum = Integer.parseInt(condition.getOrDefault("pageNum", 1).toString());
            pageSize = Integer.parseInt(condition.getOrDefault("pageSize", 5).toString());
        }
        IPage<Order> page = orderService.getOrderPageByCondition(pageNum, pageSize, condition);
        return RestResponse.success(page);
    }

    @PostMapping("/update/status/{orderIds}")
    public RestResponse<String> updateOrderStatus(@PathVariable List<Long> orderIds, @RequestParam Integer status){
        return orderService.updateStatus(orderIds, status) ?
                RestResponse.success("修改成功!")
                    : RestResponse.fail("修改失败");
    }
    @DeleteMapping("/delete/{orderIds}")
    public RestResponse<String> deleteOrders(@PathVariable List<Long> orderIds){
        return orderService.removeBatchByIds(orderIds) ?
                RestResponse.success("删除成功!")
                    :RestResponse.fail("删除失败");
    }
    @PostMapping("/query/{orderId}")
    public RestResponse<Order> queryOrder(@PathVariable Long orderId){
        return RestResponse.success(orderService.getOrderWithAllById(orderId));
    }
    @PostMapping("/insert")
    public RestResponse<Order> addOrder(@RequestBody Map<String, Object> map,
                                         HttpSession session){
        // 设置用户ID
        User user = (User) session.getAttribute("loginUser");
        map.put("buyerId", user.getId());
        Order order = orderService.addOrderByMap(map);
        return order != null ?
                RestResponse.success(order) : RestResponse.fail(null);
    }

    @PostMapping("/user/select")
    public RestResponse<List<Order>> getOrder(HttpSession session, @RequestParam(required = false) Integer status){
        User user = (User) session.getAttribute("loginUser");
        return RestResponse.success(orderService.getOrderListByUserId(user.getId(), status));
    }

    @PostMapping("/update/pay/{orderId}")
    public RestResponse<String> payOrder(@PathVariable Long orderId){
        return orderService.payOrderById(orderId) ?
                RestResponse.success("订单支付成功!")
                    : RestResponse.fail("订单支付失败.");
    }
    @PostMapping("/update/cancel/{orderId}")
    public RestResponse<String> cancelOrder(@PathVariable Long orderId){
        return orderService.cancelOrderById(orderId) ?
                RestResponse.success("订单已取消!")
                    : RestResponse.fail("订单取消失败，请联系管理员");
    }

    /**
     * 查看最近的几条订单
     * @param num
     * @return
     */
    @PostMapping("/select/current/{num}")
    public RestResponse<List<Order>> getCurrentOrders(@PathVariable Integer num){
        return RestResponse.success(orderService.getCurrentOrders(num));
    }
    @PostMapping("/selectCountMoney")
    public RestResponse<Double> countOrdersMoney(){
        return RestResponse.success(orderService.countMoney());
    }
    @PostMapping("/selectMoneyDetail")
    public RestResponse<Map<String, Object>> getMoneyByCategory(){
        return RestResponse.success(orderService.countMoneyByCategory());
    }
}
