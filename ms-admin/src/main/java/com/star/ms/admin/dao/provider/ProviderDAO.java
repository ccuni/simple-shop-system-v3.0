package com.star.ms.admin.dao.provider;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.provider.Provider;

import java.util.Map;

public interface ProviderDAO {
    public IPage<Provider> selectProductPageByMapWithType(Page<Provider> objectPage, Map<String, Object> condition);

    Provider selectOneById(Long id);
}
