package com.star.ms.admin.dao.user;

import java.util.Map;
import java.util.Set;

public interface RedisDAO {

    public long ttl(String key);
    public void expire(String key, long timeout);
    public long incr(String key, long delta);
    public Set<String> keys(String pattern) ;
    public void del(String key);
    // String（字符串）
    public void set(String key, String value);
    public void set(String key, String value, long timeout) ;
    public String get(String key);
    // Hash（哈希表）
    public void hset(String key, String field, Object value);
    public String hget(String key, String field);
    public void hdel(String key, Object... fields) ;

    public Map<Object, Object> hgetall(String key);
    // List（列表）
    public long lpush(String key, String value) ;
    public String lpop(String key);
    public long rpush(String key, String value);
}
