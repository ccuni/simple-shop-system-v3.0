package com.star.ms.admin.service.order;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.pojo.Order;

import java.util.List;
import java.util.Map;


public interface OrderService extends IService<Order> {

    IPage<Order> getOrderPageByCondition(int pageNum, int pageSize, Map<String, Object> condition);

    boolean updateStatus(List<Long> orderIds, Integer status);

    Order getOrderWithAllById(Long orderId);

    Order addOrderByMap(Map<String, Object> map);

    List<Order> getOrderListByUserId(Long userId, Integer status);

    boolean payOrderById(Long orderId);

    boolean cancelOrderById(Long orderId);

    List<Order> getCurrentOrders(Integer num);

    Double countMoney();

    Map<String, Object> countMoneyByCategory();
}
