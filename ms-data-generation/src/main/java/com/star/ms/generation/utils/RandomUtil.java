package com.star.ms.generation.utils;

import com.star.ms.common.pojo.user.User;
import com.star.ms.common.utils.FileUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class RandomUtil {
    private static final int[] ascii = {48, 97, 65};
    private static final int[] limit = {10, 26, 26};
    private static final String[] suffix = {"@qq.com", "@yeah.net", "@163.com"};
    private static final Random random = new Random();
    private static final String[] tels = {"131", "132", "133", "153", "155", "156", "182", "183"};
    private static String[] usernames;
    public static Set<String> randomEmail(int size, int len){
        Set<String> set = new HashSet<>();
        // 数字 0 ~ 9, 字母 a-z、A-Z
        while (set.size() < size) {
            StringBuilder sb  = new StringBuilder();
            for (int i1 = 0; i1 < len; i1++) {
                // 取数字 或 字母随机
                int k = random.nextInt(27) % 3;
                // 取随机的字符
                sb.append((char) (ascii[k] + random.nextInt(27) % limit[k]));
            }
            // 取随机邮箱后加入后缀
            set.add(sb.append(suffix[random.nextInt(27) % 3]).toString());
        }
        return set;
    }
    public static List<String[]> randomImg(List<String[]> list, int size){
        List<String[]> result = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            result.add(list.get(random.nextInt(list.size())));
        }
        return result;
    }
    public static String randomNumber(int len){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            sb.append(random.nextInt(10) % 10);
        }
        return sb.toString();
    }
    public static Set<String> randomTel(int size){
        Set<String> set = new HashSet<>();
        while(set.size() < size){
            String tel = "";
            tel = tels[random.nextInt(tels.length)];
            set.add(tel + randomNumber(8));
        }
        return set;
    }
    public static List<User> randomUser(List<String[]> imgs, Integer size) {
        try {
            if(usernames == null)
                usernames =  FileUtil.load("randomUserName.txt").toArray(new String[0]);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        List<String> emails = new ArrayList<>(RandomUtil.randomEmail(size, 6));
        List<String> tels = new ArrayList<>(RandomUtil.randomTel(size));
        List<String[]> images = RandomUtil.randomImg(imgs, size);
        List<User> list = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            User user = new User();
            user.setEmail(emails.get(i));
            user.setTel(tels.get(i));
            user.setUsername(usernames[i]);
            try {
                user.setBirthday(DateUtil.getRandomDate("1970-01-01", "2005-01-01"));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            user.setPassword("123456");
            user.setGender(random.nextInt(2));
            user.setUsercode(String.format("user%03d", i));
            user.setImg(images.get(i)[1]);
            // 设置普通用户的角色
            list.add(user);
        }
        return list;
    }

}
