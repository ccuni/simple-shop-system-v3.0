package com.star.ms.generation.controller;

import com.star.ms.admin.dao.mongodb.ReviewDAO;
import com.star.ms.admin.service.mongodb.ReviewService;
import com.star.ms.admin.service.product.ProductService;
import com.star.ms.common.pojo.RestResponse;
import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.user.User;
import com.star.ms.common.service.OssService;
import com.star.ms.generation.service.RandomService;
import com.star.ms.generation.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/random")
public class RandomController {
    @Autowired
    private OssService oss;
    private List<String[]> data;
    @Autowired
    private RandomService randomService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ReviewDAO reviewDAO;
    @Autowired
    private ReviewService reviewService;
    @Autowired
    private Neo4jService neo4jService;
    private String[] jsonFileName = {"json/1_1.json", "json/1_2.json", "json/2_1.json", "json/2_2.json","json/3_1.json", "json/3_2.json",
            "json/4_1.json", "json/4_2.json", "json/5_1.json", "json/5_2.json", "json/6_1.json", "json/6_2.json", "json/7_1.json", "json/7_2.json", "json/8_1.json", "json/8_2.json"};
    @GetMapping("/img/{nums}")
    public RestResponse<List<String[]>> randomImg(@PathVariable Integer nums){
        if(data == null) data = oss.getDefaultHeadImgLink();
        return RestResponse.success(RandomUtil.randomImg(data, nums));
    }
    @GetMapping("/email/{nums}")
    public RestResponse<Set<String>> randomEmail(@PathVariable Integer nums){
        return RestResponse.success(randomService.randomEmail(nums));
    }
    @GetMapping("/tel/{nums}")
    public RestResponse<Set<String>> randomTel(@PathVariable Integer nums){
        return RestResponse.success(RandomUtil.randomTel(nums));
    }
    @GetMapping("/user/{nums}")
    public RestResponse<List<User>> randomUser(@PathVariable Integer nums){
        if(data == null) data = oss.getDefaultHeadImgLink();
        return RestResponse.success(randomService.randomUser(data, nums));
    }

    @GetMapping("/address/{nums}")
    public RestResponse<List<String>> randomAddress(@PathVariable Integer nums){
        return RestResponse.success(randomService.randomAddress(nums));
    }
    @GetMapping("/user/insert/{nums}")
    public RestResponse<Boolean> insertUser(@PathVariable Integer nums){
        if(data == null) data = oss.getDefaultHeadImgLink();
        return RestResponse.success(randomService.saveRandomUser(data, nums));
    }

    @GetMapping("/product")
    public RestResponse<List<Product>> randomProduct(){

        return RestResponse.success(randomService.randomProductFormat(jsonFileName));
//        return RestResponse.success(randomService.randomProductFormat("json/1_1.json", "json/1_2.json", "json/2_1.json", "json/2_2.json"));
    }
    @GetMapping("/product/insert")
    public RestResponse<Boolean> insertProduct(){
        List<Product> list = randomService.randomProductFormat(jsonFileName);
        return RestResponse.success(productService.saveBatch(list));
    }
    // 获取JSON中所有的评价信息

    // 删除所有的评论
    @GetMapping("/reviews/delete/all")
    public RestResponse<Object> removeReviews(){
        return RestResponse.success(randomService.removeReviews());
    }
    @GetMapping("/reviews/query")
    public RestResponse<List<Review>> getReviews(){
//        return RestResponse.success(GeneratorUtil.getReviewsFromJson(jsonFileName)); 未格式化的
        return RestResponse.success(randomService.randomReview(jsonFileName));
    }

    @PostMapping("/reviews/insert/all")
    public RestResponse<List<Review>> insertReviews(){
//        return RestResponse.success(GeneratorUtil.getReviewsFromJson(jsonFileName)); 未格式化的
        return RestResponse.success(randomService.randomReviewAndInsert(jsonFileName));
    }
    @GetMapping("/reviews/query/mongodb")
    public RestResponse<List<Review>> getReviewsFromMongoDB(){
//        return RestResponse.success(GeneratorUtil.getReviewsFromJson(jsonFileName)); 未格式化的
        return RestResponse.success(reviewDAO.selectList());
    }
    @GetMapping("/reviews/query/mongodb/{productId}")
    public RestResponse<List<ReviewVo>> getReviewsFromMongoDB(@PathVariable Long productId){
//        return RestResponse.success(reviewService.getReviewListByProductId(productId)); 缺少用户信息
        return RestResponse.success(reviewService.getReviewVoListByProductId(productId));
    }

    // 查询指定用户的所有评价
    @GetMapping("/user/query/{userId}")
    public RestResponse<List<Review>> getReviewsFromMongoDBByUserId(@PathVariable Long userId){
        return RestResponse.success(reviewService.getReviewVoListByUserId(userId));
    }

    // 根据 MongoDB 插入所有的用户、商品、评论节点
    @PostMapping("/node/insert")
    public RestResponse<String> insertAllNodes(){
        boolean result = neo4jService.initNodes();
        return result ? RestResponse.success("插入成功") : RestResponse.fail("插入失败");
    }
}
