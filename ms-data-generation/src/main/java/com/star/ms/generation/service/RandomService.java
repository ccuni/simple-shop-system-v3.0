package com.star.ms.generation.service;

import com.star.ms.common.pojo.product.Product;
import com.star.ms.common.pojo.user.User;
import com.star.ms.generation.dto.ProductDto;

import java.util.List;
import java.util.Set;

public interface RandomService {
    public List<User> randomUser(List<String[]> imgs, Integer size) ;

    public Set<String> randomEmail(int num);    // 生成随机的邮箱

    public List<String> randomAddress(int size);        // 生成随机的地址

    boolean saveRandomUser(List<String[]> imgs, Integer nums);

    public List<ProductDto> randomProduct(int size);
    public List<Product> randomProductFormat(String ...fileName);

    ///// 产生评论
    public List<Review> randomReview(String ...fileName);
    public List<Review> randomReviewAndInsert(String ...jsonFileName);

    public boolean removeReviews();
}
