package com.star.ms.generation.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/***
 * 产生随机的时间
 */
public class DateUtil {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public static Date getRandomDate(String startDate, String endDate) throws ParseException {
        long start = sdf.parse(startDate).getTime();
        long end = sdf.parse(endDate).getTime();
        // 随机产生一个日期
        long date  = start + new Double(Math.random() * (end - start)).longValue();
        return new Date(date);
    }
    public static long getRandomTime(String startDate, String endDate) throws ParseException {
        return getRandomDate(startDate, endDate).getTime();
    }

    public static String getRandomDateString(String startDate, String endDate) throws ParseException {
        return getRandomDate(startDate, endDate).toString();
    }

    public static Date format(String date){
        try {
            return new Date(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date).getTime());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
