package com.star.ms.generation.dto;

import com.star.ms.generation.utils.DateUtil;
import lombok.Data;

import java.util.Date;

@Data
public class ProductDto {
    private Long product_id;    // 商品ID
    private String product_name;    // 商品名称
    private String product_detail;  // 商品描述
    private Double product_price;    // 商品价格
    private String review_product_img;           // 商品图片地址
    private String reviews_user_name;  //   评价用户名
    private String reviews_info;    // 评价内容
    private String reviews_rank;    // 评价等级
    private Date reviews_time;  // 评价时间
    public void setReviews_time(String reviews_time) {
        this.reviews_time = DateUtil.format(reviews_time);
    }

}
