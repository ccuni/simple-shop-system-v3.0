package com.star.ms.generation;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.star.ms"})
@MapperScan("com.star.ms.common.dao.mapper")
public class Generator {

    public static void main(String[] args) {
        SpringApplication.run(Generator.class, args);
    }
}
