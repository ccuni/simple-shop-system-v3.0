package com.star.ms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/*
 * @Author: Uni
 * @Time: 2022/5/12
 * @TODO
 */
@Controller
public class DispatcherController {
    @GetMapping({"/index.html"})
    public String toIndex(){
        return "index";
    }
    @GetMapping({"/home.html", "/"})
    public String toHome() {return "home";}
    @GetMapping("/register.html")
    public String toRegister(){
        return "register";
    }

    @GetMapping("/retrieve.html")
    public String toRetrieve() {return "retrieve";}
    @GetMapping("/login.html")
    public String toTest(){return "login";}

    @GetMapping("/user-center.html")
    public ModelAndView toUserCenter(ModelAndView model, @RequestParam(required = false) String type){
        if(type != null)
            model.addObject("type", type);
        else
            model.addObject("type", "base");
        model.setViewName("user/user-center");
        return model;
    }
    @GetMapping("/product.html")
    public String toProduct(){
        return "product/detail";
    }
    @GetMapping("/alipay")
    public String toTestAliPay(){
        return "testAliApi";
    }
    @GetMapping("/admin")
    public String toAdmin(){
        return "admin/admin-center";
    }
    @GetMapping("/admin.html")
    public String toAdminUser(Model model, @RequestParam String type){
        model.addAttribute("type", type);
        return "admin/admin-center";
    }

    @GetMapping("/testAlipay")
    public String toTestAlipay() { return "testAlipay";}

    @GetMapping("/noauth")
    public String toNoauth() { return "noauth"; }
}
