/**
 * 页面预加载
 */
setTimeout(function () {
    // 关闭加载类
    $('.placeholder').removeClass('placeholder')
},DELAY_LOAD_REGISTER)

const $code = $('#ms-form-reg-code')
const $password = $('#ms-form-reg-password')
const $password_second = $('#ms-form-reg-password-second')
const $tel = $('#ms-form-reg-tel')
const $email = $('#ms-form-reg-email')
const $form_register = $('#ms-form-register')
// 验证用户输入的账号
function ms_reg_check_code(){
    $errorText = $('#ms-for-reg-check-code-fail')   // 错误提示
    let errorText
    let value = $code.val()
    let reg = /^[0-9a-zA-Z]{5,11}$/
    let flag = true
    // 1. 不能为空
    if(value === '') {
        flag = false
        errorText = '提示: 账号不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 5 || value.length >= 10) {
        flag = false
        errorText = '提示: 账号长度不能超过10且不能的低于5'
    }
    // 3. 只能是数字、字母、下划线，不能包含特殊符号
    else if(reg.test(value) === false) {
        flag = false
        errorText = '提示: 账号只能是数字或字母，不能包含特殊符号'
    }
    if(flag === false) {
        $code.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $code.removeClass('is-invalid').addClass('is-valid')
    }
    return flag
}
// 验证用户输入的密码
function ms_reg_check_password(){
    $errorText = $('#ms-form-reg-check-password-fail')
    let errorText = ''
    let value = $password.val()
    let reg = /^[0-9a-zA-Z]{6,15}$/
    let flag = true
    // 1. 不能为空
    if(value === '') {
        flag = false
        errorText = '提示: 密码不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 6 || value.length >= 15) {
        flag = false
        errorText = '提示: 密码长度需在6到15的范围内'
    }
    // 3. 只能是数字或字母，不能包含特殊符号
    else if(reg.test(value) === false) {
        flag = false
        errorText = '提示: 密码只能是数字或字母，不能包含特殊符号'
    }
    if(flag === false) {
        $password.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $password.removeClass('is-invalid').addClass('is-valid')
    }
    return flag
}
// 验证用户第二次输入的密码
function ms_reg_check_password_second() {
    $errorText = $('#ms-form-reg-check-password-second-fail')
    let errorText = ''
    let flag = true
    if ($password_second.val() === '') {
        flag = false
        errorText = '提示: 确认密码不能为空'
    } else if ($password.val() !== $password_second.val()) {
        flag = false
        errorText = '提示: 两次输入的密码不一致'
    }
    if (flag === true) {
        $password_second.removeClass('is-invalid').addClass('is-valid')
    } else {
        $errorText.html(errorText)
        $password_second.removeClass('is-valid').addClass('is-invalid')
    }
    return flag
}
// 验证用户输入的联系方式
function ms_reg_check_tel(){
    $errorText = $('#ms-form-reg-check-tel-fail')
    let reg = /^1[3-9][0-9]{9}$/g
    let flag = true
    if(!reg.test($tel.val())){
        flag = false
        $errorText.html('提示: 输入的联系方式必须为13~19开头的11位数字')
        $tel.removeClass('is-valid').addClass('is-invalid')
    } else
        $tel.removeClass('is-invalid').addClass('is-valid')
    return flag
}
// 验证用户输入的邮箱
/**
 * 只允许英文字母、数字、下划线、英文句号、以及中划线组成
 * 参考: https://www.cnblogs.com/cuihongyu3503319/p/10244983.html
 * @returns {boolean}
 */
function ms_reg_check_email(){
    let email = $email.val()
    $errorText = $('#ms-form-reg-check-email-fail')
    let errorText = ''
    let reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/g
    let flag = true
    if(email.length === 0){
        flag = false
        errorText = '提示: 邮箱不能为空'
    }
    else if(email.length > 30){
        flag = false
        errorText = '提示: 邮箱的长度不能超过30'
    }
    else if(!reg.test($email.val())) {
        flag = false
        errorText = '提示: 输入的邮箱必须符合xxx@xxx.xxx的格式，且不超过30个字符'
    }
    if (flag === false){
        $errorText.html(errorText)
        $email.removeClass('is-valid').addClass('is-invalid')
    } else
        $email.removeClass('is-invalid').addClass('is-valid')
    return flag
}
$code.blur(ms_reg_check_code)
$password.blur(ms_reg_check_password)
$password_second.blur(ms_reg_check_password_second)
$tel.blur(ms_reg_check_tel)
$email.blur(ms_reg_check_email)


function checkForm($form, ...expect){
    let flag = true
    $.each($form.serializeArray(), function (index, elem){
        // 排除过滤的标签
        if(expect != null && expect.indexOf(elem['name']) >= 0)
            return true
        if(elem['value'] === ''){
            // 获取空值的input属性
            flag = $('input[name="' + elem['name'] + '"]').siblings('label').text()
            return false
        }
    })
    return flag
}
function send_email_code(btn_id){
    ms_btn_disable(btn_id, '发送中')
    let checkResult = checkForm($('#ms-form-register'), REG_EXCEPT_INPUT_EMAIL_CODE)
    // 检验表单
    if(checkResult !== true){   // 表单不过关，返回 false
        ms_show_toast_callback(false, checkResult + ' 不能为空! ', function (){
            ms_btn_enable(btn_id)
        })
        return false;
    }
    // 请求发送验证码
    $.ajax({
        url: '/user/sendcode',
        type: 'POST',
        data: $form_register.serialize(),
        dataType: 'json',
        success: function (result){
            console.log(result)
            if(result['code'] === CODE_SUCCESS){
                ms_show_toast(true, result['msg'])
                // 加载倒计时按钮
                ms_btn_code_wait(btn_id)
            } else
                ms_show_toast(false, result['msg'])
        },
        error: function (error){
            console.log(error)
            ms_show_toast(false, "发送失败，请检查邮箱是否正确!")
            ms_btn_enable(btn_id, '发送')
        }
    })
}

function ms_register(btn_id){
    // 检查表单是否合理
    if(!($code.blur() && $password.blur() && $password_second.blur() && $tel.blur() && $email.blur())) return
    ms_btn_disable(btn_id, '注册中')
    // 基于JQuery3.x版本的AJAX模板
    setTimeout(function (){
        $.ajax({
            url: '/user/register',               // 请求的controller
            type: 'POST',                       // 请求方式
            data: $form_register.serialize(),   // 传递表单的信息$ ('#form-msRegister').serialize()
            dataType: 'json',                   // 返回的数据类型
            success: function(result) {         // 返回成功后
                ms_btn_enable(btn_id, '注册')
                console.log(result)
                if(result['code'] === CODE_SUCCESS) {
                    // 注册成功 -> 显示提示窗，显示后跳转到首页
                    ms_show_toast_callback(true, result['msg'], function (){
                        window.location = '/login.html'
                    })
                }
                else
                    ms_show_toast(false, result['msg'])
            },
            error: function(error){     // 若返回错误，则执行这个函数
                ms_show_toast(false, '注册失败, 服务端内部出错')
                // 控制台打印错误
                console.log(error)
            }
        })
    }, DELAY_REGISTER)
}

/**
 * 随机生成并填入合法的用户信息（测试专用）
 */
function ms_register_random(){
    $code.val(randomCode())
    $password.val(randomPassword())
    $password_second.val($password.val())
    $tel.val(randomTel())
    $email.val(randomEmail())

    $code.blur()
    $password.blur()
    $password_second.blur()
    $tel.blur()
    $email.blur()

}