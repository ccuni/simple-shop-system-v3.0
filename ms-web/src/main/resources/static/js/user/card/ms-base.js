setTimeout(function (){
    $('.placeholder').removeClass('placeholder')
}, DELAY_LOAD_USER_BASE)
/**
 * 获取用户的数据
 */
let user_address = []
$(document).ready(function (){
    renderAddress(ajaxPostAddressByUserCode($('#ms-form-user-base-code').val()))
})
// 基本信息的表单
$form_user_base = $('#ms-form-user-base')
// 基本信息表单的输入框
$base_username = $('#ms-form-user-base-username')
var flag_username = false
$base_tel = $('#ms-form-user-base-tel')
var flag_tel = false
$base_birthday = $('#ms-form-user-base-birthday')
var flag_birthday = false
$base_address = $('#ms-form-user-base-address')
var flag_address = false

// 用户名验证
$base_username.blur(function (){
    $errorText = $('#ms-form-user-base-username-fail')   // 错误提示
    let errorText = ''
    let value = $base_username.val()
    let reg = /^[0-9a-zA-Z\u4e00-\u9fa5]{2,11}$/
    flag_username = true
    // 1. 不能为空
    if(value === '') {
        flag_username = false
        errorText = '提示: 用户名不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 2 || value.length >= 10) {
        flag_username = false
        errorText = '提示: 用户名长度不能超过10且不能的低于2'
    }
    // 3. 只能是数字、字母、下划线，不能包含特殊符号
    else if(reg.test(value) === false) {
        flag_username = false
        errorText = '提示: 用户名只能是汉字、数字或字母，不能包含特殊符号'
    }
    if(flag_username === false) {
        $base_username.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $base_username.removeClass('is-invalid').addClass('is-valid')
    }
})
// 联系方式验证
$base_tel.blur(function (){
    $errorText = $('#ms-form-user-base-tel-fail')
    let reg = /^1[3-9][0-9]{9}$/g
    let value = $base_tel.val()
    let errorText = ''
    flag_tel = true
    if(value == ''){
        flag_tel = false
        errorText = '提示: 联系方式不能为空'
    } else if(!reg.test(value)) {
        flag_tel = false
        errorText = '提示: 输入的联系方式必须为13~19开头的11位数字'
    }
    if(flag_tel === false){
        $errorText.html(errorText)
        $base_tel.removeClass('is-valid').addClass('is-invalid')
    } else
        $base_tel.removeClass('is-invalid').addClass('is-valid')
})

// 生日验证
$base_birthday.blur(function (){
    flag_birthday = true
    if($base_birthday.val() == ''){
        flag_birthday = false
        $(this).removeClass('is-valid').addClass('is-invalid')
    } else
        $(this).removeClass('is-invalid').addClass('is-valid')
})

// 详细地区验证
$base_address.blur(function (){
    flag_address = true
    let $errorText = $('#ms-form-user-base-address-fail')
    let errorText = ''
    let value = $base_address.val()
    if(value == ''){
        flag_address = false
        errorText = '提示: 详细地区不能为空'
    } else if(value.length >= 128){
        flag_address = false
        errorText = '提示: 详细地区不能超过128个字符,目前已达到: ' + value.length
    }

    if(flag_address === false){
        $errorText.html(errorText)
        $(this).removeClass('is-valid').addClass('is-invalid')
    } else
        $(this).removeClass('is-invalid').addClass('is-valid')
})
function checkBaseForm(){
    $base_username.blur()
    $base_tel.blur()
    $base_birthday.blur()
    $base_address.blur()
    return flag_username === true
        && flag_tel === true && flag_birthday === true && flag_address === true
}
function saveUserBase(btn_id){
    let check = checkBaseForm()
    // 检验表单
    if(check === false) {
        ms_show_toast(false, '请检查输入的信息是否符合要求.')
        return true;
    }
    ms_btn_disable(btn_id, '保存中')
    setTimeout(function (){
        $.ajax({
            url: '/user/update/self',
            type: 'POST',
            data: $form_user_base.serialize(),
            dataType: 'json',
            error: function (err) {
                ms_show_toast(false, err.responseText)
                console.error(err.responseText)
            },
            success: function (result){
                if(result['code'] === CODE_SUCCESS){
                    ms_btn_enable(btn_id, '保存')
                    ms_show_toast_callback(true, result['msg'], function (){
                        // 刷新页面
                        window.location.reload()
                    })
                } else
                    ms_show_toast(false, result['msg'])
            }
        })
    }, DELAY_TOAST_WHEN_BTN)

}