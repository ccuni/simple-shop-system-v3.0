/**
 * 退出登录
 */
function exitLogin(nextPage){
    setTimeout(function (){
        $.ajax({
            url: '/user/logout',
            type: 'POST',
            dataType: 'json',
            success: function (result){
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast_callback(true, result['msg'], function (){
                        window.location = nextPage
                    })
                } else
                    ms_show_toast(false, result['msg'])
            },
            error: function (error){
                ms_show_toast(false, '退出失败!')
                console.error(error)
            }
        })
    },DELAY_EXIT_LOGIN)
}
