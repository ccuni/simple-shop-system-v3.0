// 配置全局参数

// 响应的编码
const CODE_SUCCESS = 200 // 响应成功
const CODE_SUCCESS_LOGOUT = 201 // 响应成功，但是要求重新登录

// 按钮点击后的延迟（单位: 毫秒)
const DELAY_REGISTER = 500
const DELAY_LOGIN = 750
// 弹窗出现后的消失延迟
const DELAY_TOAST_SUCCESS = 800
const DELAY_TOAST_FAIL = 800
// 弹窗和按钮的间隔
const DELAY_TOAST_WHEN_BTN = 500
// 退出登录的延迟
const DELAY_EXIT_LOGIN = 300
// 页面预加载延迟
    // 登录页面的预加载延迟
const DELAY_LOAD_LOGIN = 200
    // 注册页面的预加载延迟
const DELAY_LOAD_REGISTER = 200
const DELAY_LOAD_INDEX_WAVE = 300
const DELAY_LOAD_INDEX = 200
// 点击发送验证码后 ，验证码的禁止时间（单位: 毫秒)
const BTN_LAST_TIME = 60000


// 在注册页面时，排除验证的 input标签
const REG_EXCEPT_INPUT_EMAIL_CODE = 'emailCode'

// ========================BEGIN 随机生成的相关配置====================

// 用户账号的长度范围
const LIMIT_USER_CODE_MIN = 5
const LIMIT_USER_CODE_MAX = 10
// 用户密码的长度范围
const LIMIT_USER_PASSWORD_MIN = 6
const LIMIT_USER_PASSWORD_MAX = 15

// 随机邮箱的域名
const RANDOM_EMAIL_HOSTNAME = ['@qq.com', '@163.com', '@126.com', '@year.net']
const RANDOM_EMAIL_LEN = 5
// ========================END 随机生成的相关配置====================



// ========================BEGIN 验证码 ms-captcha.js 的相关配置 =======================
// 验证码图片的干扰点个数
const CAPTCHA_NUM_INTERFERE_POINT = 30
// 验证码图片的干扰线点个数
const CAPTCHA_NUM_INTERFERE_LINE = 2
// 当前的验证码
var captcha_trueth = '0'

//=============================== BEGIN 用户信息修改相关的延迟配置 ==========================
const DELAY_EMAIL_NEXT = 200
const DELAY_LOAD_USER_BASE = 200
const DELAY_LOAD_USER_HEAD = 200
const DELAY_LOAD_USER_HEAD_WAVE = 200
const DELAY_LOAD_USER_PASSWORD = 200
const DELAY_LOAD_USER_EMAIL = 200

//==================================END 用户信息修改相关的延迟配置 ==========================
// 商品出现的延迟
const DELAY_INDEX_PRODUCT_APPEARED = 300

//============================== BEGIN 商品 页面 =================================
// 商品出现的延迟
const DELAY_PRODUCT_APPEARED = 200
// 商品图片出现的延迟
const DELAY_PRODUCT_APPEARED_WAVE = 300

// ============================== 默认图片配置 ================================//

const DEFAULT_IMG_INSERT_PRODUCT = 'https://uni1024.oss-cn-hangzhou.aliyuncs.com/mall-system/public/%E6%B7%BB%E5%8A%A0%E5%95%86%E5%93%81.png'


// =============================== 订单状态 ==================================//
ORDER_STATUS = ['待结算', '待发货', '运输中', '待确认收货', '已收货']
ORDER_STATUS_MAP = {'待结算': 0, '待发货':1, '运输中':2, '待确认收货':3, '已收货':4}