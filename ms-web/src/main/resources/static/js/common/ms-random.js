/**
 * 生成随机长度的字符串
 * @param len 字符串长度
 */
function randomNumberOrLetter(len){
    // 至少要为1位
    len = len || 1;
    // 第一位为字母
    let s = String.fromCodePoint(97 + parseInt(Math.random() * 26))
    for (let i = 0; i < len - 1; i++){
        // 决定是数字还是字母
        if(Math.random() % 2){          // 数字
            s += Math.floor(Math.random() * 9)
        } else                          // 字母
            s += String.fromCodePoint(97 + parseInt(Math.random() * 26))
    }
    return s
}

/**
 * 随机生成数字
 * @param len
 */
function randomNumber(len){
    let num = 1
    // 长度小于或等于1 直接返回一位的随机数字
    if(len <= 1) return parseInt(Math.random()  * 10)

    for(let i = 0; i <= len; i++){
        num = num * 10 + parseInt(Math.random() * 10)
    }
    return num
}

/**
 * 生成随机的用户账号
 */
function randomCode(){
    // 获取在限制的[min , max]范围内的一个长度
    let len =parseInt(Math.random() * (LIMIT_USER_CODE_MAX - LIMIT_USER_CODE_MIN+1) + LIMIT_USER_CODE_MIN)
    return randomNumberOrLetter(len)
}


/**
 * 生成随机的密码
 */
function randomPassword(){
    // 获取在限制的[min , max]范围内的一个长度
    let len =parseInt(Math.random() * (LIMIT_USER_PASSWORD_MAX - LIMIT_USER_PASSWORD_MIN+1) + LIMIT_USER_PASSWORD_MIN)
    return randomNumberOrLetter(len)
}

/**
 * 生成随机的联系电话
 */
function randomTel(){
    // 电话是以13~19 开头的
    let tel = Math.ceil(Math.random() * (19 - 13))+ 13
    // 接下来生成随机的9位数字即可
    tel += randomNumber(9)
    return tel
}

/**
 * 生成随机的邮箱
 */
function randomEmail(){
    let n = RANDOM_EMAIL_HOSTNAME.length
    let email = ''
    // 前缀
    email = randomNumberOrLetter(RANDOM_EMAIL_LEN)
    // 后缀
    email += RANDOM_EMAIL_HOSTNAME[parseInt(Math.random() * n)]
    return email
}
