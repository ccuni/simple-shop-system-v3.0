// 获取URL的第一个参数所包含的URL
function paramGetUrLWithFirst(){
    return document.location.href.split('&')[0]
}

function paramGetURL(thisURL, name) {
    let array = new RegExp(name + '=([^&]*)').exec(thisURL);
    return (array == null || array.length < 1)? '' : decodeURI(array[1]);
}
function paramDeleteCurrentURLNot(name){
    let thisURL = decodeURI(location.href).replace('#', '')
    // 如果 url中包含这个参数 则删除
    if (thisURL.indexOf(name+'=') > 0) {
        var v = paramGetURL(thisURL, name);
        if (v != null) {
            // 是否包含参数
            thisURL = thisURL.replace('&'+name + '=' + v, '');
            thisURL = thisURL.replace('?'+name + '=' + v, '');

        }
        else {
            thisURL = thisURL.replace('&' + name + '=', '');
            thisURL = thisURL.replace('?' + name + '=', '');
        }
    }
    return thisURL
}
function paramDeleteCurrentURL(name){
    location.href = paramDeleteCurrentURLNot(name)
}
function paramUpdateURL(thisURL, name, val) {
    thisURL = decodeURI(thisURL).replace('#', '')
    // 如果 url中包含这个参数 则修改
    if (thisURL.indexOf(name+'=') > 0) {
        var v = paramGetURL(thisURL, name);
        if (v != null) {
            // 是否包含参数
            thisURL = thisURL.replace(name + '=' + v, name + '=' + val);
        }
        else {
            thisURL = thisURL.replace(name + '=', name + '=' + val);
        }

    } // 不包含这个参数 则添加
    else {
        if (thisURL.indexOf("?") > 0) {
            thisURL = thisURL + "&" + name + "=" + val;
        }
        else {
            thisURL = thisURL + "?" + name + "=" + val;
        }
    }
    return thisURL;
}
function clearURL(){
    let url = location.href
    url = decodeURI(url).replace('#', '')
    let type = paramGetURL(url, 'type')
    if(type !== '')
        location.href = url.split('?')[0] + '?type=' + type
    else
        location.href = url.split('?')[0]
}
function paramUpdateCurrentParam(name, val) {
    return paramUpdateURL(document.location.href, name, val)
}
// 修改多个参数
function paramUpdateCurrentParams(names, vals) {
    let url = document.location.href
    if(names.length === vals.length){
        for(let i = 0; i < names.length; i++)
            url = paramUpdateURL(url, names[i], vals[i])
    }
    return url
}
function paramGetURLtoMap() {
    // 首先去掉 #
    let params = window.location.toString().replace('#', '')
    // let params = window.location.toString()
    params = params.split('\?')[1]
    let map = new Map();
    if(params == null || params === '') return ''
    params.split('&').forEach(param =>{
        let kv = param.split('=');
        let k = kv[0]
        let v = kv[1]
        map.set(k,v)
    })
    return map
}
function paramGetURLtoJSON() {
    let obj= Object.create(null)
    let map = paramGetURLtoMap();
    if(map == null || map === '') return;
    for (let[k,v] of map) {
        k = decodeURI(k)
        v = decodeURI(v)
        obj[k] = v;
    }
    return JSON.stringify(obj)
}
